﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MvcTraining01.Models
{
    [DataContract]
    public sealed class SalesRep : User
    {
        public string SalesRepId { get; set; }

        public override UserRole Role
        {
            get
            {
                return UserRole.SalesRep;
            }
        }

        public SalesRep(string userName, string firstName, string lastName, string email, string salesRepId) : base(userName, firstName, lastName, email)
        {
            this.SalesRepId = salesRepId;
        }
    }
}