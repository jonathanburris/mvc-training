﻿using Faker.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTraining01.Models.DataProviders
{
    public class MockCustomersDataProvider : ICustomersDataProvider
    {
        public List<Customer> GetCustomers()
        {
            List<Customer> customers = new List<Customer>() {
                new Customer(Names.FullName()),
                new Customer(Names.FullName()),
                new Customer(Names.FullName()),
                new Customer(Names.FullName())
            };

            return customers;
        }

        public Customer GetCustomer(int id)
        {
            throw new NotImplementedException();
        }

        public void CreateCustomer()
        {
            throw new NotImplementedException();
        }

        public void UpdateCustomer()
        {
            throw new NotImplementedException();
        }

        public void DeleteCustomer()
        {
            throw new NotImplementedException();
        }
    }
}