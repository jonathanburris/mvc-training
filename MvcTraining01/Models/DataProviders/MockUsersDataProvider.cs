﻿using MvcTraining01.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTraining01.Models.DataProviders
{
    public class MockUsersDataProvider : MvcTraining01.Models.DataProviders.IUsersDataProvider
    {
        public Users GetUsers()
        {
            Users users = new Users();

            int i = 0;
            while (i < 10)
            {
                users.Add(new Developer(
                Faker.Generators.Strings.GenerateAlphaNumericString(),
                Faker.Generators.Names.First(),
                Faker.Generators.Names.Last(), 
                Faker.Generators.EmailAddresses.Human()));

                i++;
            }

            return users;
        }

        public User GetUser(string username)
        {
            Users users = this.GetUsers();
            User user = users.Where(u => u.UserName == username).FirstOrDefault();
            return user;
        }
    }
}