﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvcTraining01.Models.DataProviders
{
    public interface ICustomersDataProvider
    {
        List<Customer> GetCustomers();
        Customer GetCustomer(int id);
        void CreateCustomer();
        void UpdateCustomer();
        void DeleteCustomer();
    }
}
