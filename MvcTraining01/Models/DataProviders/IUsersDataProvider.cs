﻿using System;
namespace MvcTraining01.Models.DataProviders
{
    public interface IUsersDataProvider
    {
        Users GetUsers();
        User GetUser(string username);
    }
}
