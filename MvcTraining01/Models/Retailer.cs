﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MvcTraining01.Models
{
    [DataContract]
    public sealed class Retailer : User
    {
        public string CustomerId { get; set; }

        public override UserRole Role
        {
            get
            {
                return UserRole.Retailer;
            }
        }

        public Retailer(string userName, string firstName, string lastName, string email, string customerId) : base(userName, firstName, lastName, email)
        {
            this.CustomerId = customerId;
        }
    }
}