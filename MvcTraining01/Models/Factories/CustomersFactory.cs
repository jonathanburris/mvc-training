﻿using MvcTraining01.Models.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTraining01.Models.Factories
{

    public static class CustomersFactory
    {
        public static ICustomersDataProvider provider = new MockCustomersDataProvider();

        public static List<Customer> GetCustomers()
        {
            return provider.GetCustomers();
        }

        public static List<Customer> GetCustomers(ICustomersDataProvider provider)
        {
            return provider.GetCustomers();
        }

        public static List<Customer> GetCustomers(DataProviderSource source)
        {
            switch (source)
            {
                case DataProviderSource.Mock:
                    return GetCustomers(new MockCustomersDataProvider());
                    break;
                case DataProviderSource.Sql:
                    return GetCustomers(new SqlCustomersDataProvider());
                    break;
                default:
                    break;
            }

            return GetCustomers();
        }
    }
}