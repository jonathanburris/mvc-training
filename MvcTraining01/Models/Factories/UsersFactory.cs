﻿using MvcTraining01.Models.DataProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcTraining01.Models.Factories
{

    public static class UsersFactory
    {
        public static IUsersDataProvider provider = new MockUsersDataProvider();

        public static Users GetUsers()
        {
            return getUsers(provider);
        }

        public static Users GetUsers(DataProviderSource source)
        {
            switch (source)
            {
                case DataProviderSource.Mock:
                    return getUsers(new MockUsersDataProvider());
                    break;
                default:
                    break;
            }

            return GetUsers();
        }

        private static Users getUsers(IUsersDataProvider provider)
        {
            return provider.GetUsers();
        }

        public static User GetUser(string username)
        {
            return getUser(provider, username);
        }

        public static User GetUser(DataProviderSource source, string username)
        {
            switch (source)
            {
                case DataProviderSource.Mock:
                    return getUser(new MockUsersDataProvider(), username);
                    break;
                case DataProviderSource.Sql:
                    break;
                default:
                    break;
            }

            return GetUser(username);
        }

        private static User getUser(IUsersDataProvider provider, string username)
        {
            return provider.GetUser(username);
        }
    }
}