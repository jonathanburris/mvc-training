﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MvcTraining01.Models
{
    public enum UserRole
    {
        Administrator,
        Developer,
        Retailer,
        SalesRep        
    }

    [CollectionDataContract]
    public class Users : List<User>
    {
        public Users() { }
        public Users(List<User> users) : base(users) { }
    }

    [DataContract]
    [KnownType(typeof(Retailer))]
    [KnownType(typeof(SalesRep))]
    [KnownType(typeof(Developer))]
    [KnownType(typeof(Administrator))]
    public abstract class User : IUser
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        [Required]
        public string Country { get; set; }

        public string DisplayName { get { return string.Format("{0} {1}", FirstName, LastName); } }

        public abstract UserRole Role { get; }

        public User(string userName, string firstName, string lastName, string email)
        {
            this.UserName = userName;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
        }
    }
}