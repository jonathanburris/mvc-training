﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MvcTraining01.Models
{
    [DataContract]
    public sealed class Administrator : User
    {
        public override UserRole Role
        {
            get { return UserRole.Administrator; }
        }

        public Administrator(string userName, string firstName, string lastName, string email) : base(userName, firstName, lastName, email) { }
    }
}