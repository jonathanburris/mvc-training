﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace MvcTraining01.Models
{
    [DataContract]
    public class Customer
    {
        [Required(ErrorMessage="Customer name is required"),
        MinLength(10),
        MaxLength(50)]
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }

        //public Customer() { }

        public Customer(string customerName)
        {
            this.CustomerName = customerName;
        }

        public Customer(string customerName, string phone, string fax, string mobile)
        {
            this.CustomerName = customerName;
            this.Phone = phone;
            this.Fax = fax;
            this.Mobile = mobile;
        }

    }
}