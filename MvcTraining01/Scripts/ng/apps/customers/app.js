﻿var customers = angular.module('customers', ['global']);

customers.run(['$rootScope', '$sessionStorage', function ($rootScope, $sessionStorage) {

    $rootScope.CacheStatus = {
        Disabled: 0,
        Enabled: 1
    };

    $rootScope.$storage = $sessionStorage.$default({
        apiUrl: 'api/v1/',
        appCacheStatus: $rootScope.CacheStatus.Enabled,
        helloWorldText: 'hello world'
    });

}]);

customers.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        
        .state('root', {
            url: '',
            templateUrl: 'partials/home.html',
            controller: 'HomeController'
        })
        .state('/', {
            url: '/',
            templateUrl: 'partials/home.html',
            controller: 'HomeController'
        })
        .state('/customers', {
            url: '/customers',
            templateUrl: 'partials/customers.html',
            controller: 'CustomersController'
        })
        .state('/customers/:customerId', {
            url: '/customers/:customerId',
            templateUrl: 'partials/customer.html',
            controller: 'CustomerController'
        })
        .state('/customers/:customerId/orders', {
            url: '/customers/:customerId/orders',
            templateUrl: 'partials/customerorders.html'
        })
        .state('test route', {
            url: '/test',
            templateUrl: 'partials/test.html',
            controller: 'TestController'
        });

    $urlRouterProvider.otherwise('/');

    //$locationProvider.html5Mode(true)

});