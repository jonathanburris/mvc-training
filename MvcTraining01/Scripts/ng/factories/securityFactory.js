﻿var SecurityFactory = global.factory('SecurityFactory', ['$http', '$rootScope','$sessionStorage', function ($http, $rootScope, $sessionStorage) {

    return {

        getCurrentUsername: function () {

            var promise = $http.get($sessionStorage.apiUrl + '/security/currentusername');

            return promise;

        }

    };

}]);