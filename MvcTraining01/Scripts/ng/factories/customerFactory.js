﻿var CustomerFactory = global.factory('CustomerFactory', ['$q', '$http', '$rootScope', '$sessionStorage', function ($q, $http, $rootScope, $sessionStorage) {

    return {

        getCustomers: function (useMock) {

            var promise = $http.get($sessionStorage.apiUrl + 'customers');

            return promise;

        }

    };

}]);