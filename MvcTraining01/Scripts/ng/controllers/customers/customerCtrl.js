﻿var customerController = customers.controller('CustomerController', ['$scope', '$rootScope', '$stateParams', 'CustomerFactory', 'SecurityFactory', function ($scope, $rootScope, $stateParams, CustomerFactory, SecurityFactory) {

    $scope.CustomerFactory = CustomerFactory;
    $scope.SecurityFactory = SecurityFactory;

    $scope.customerId = $stateParams.customerId;

    $scope.init = function () {
        
        $scope.getCurrentUserName();
    }

    $scope.getCurrentUserName = function () {

        var promise = $scope.SecurityFactory.getCurrentUsername();

        promise.then(function (result) {

            $scope.currentUsername = result.data;

        },
        function (error) {

            $scope.error = error.message;

        });

    }

}]);