﻿var customersController = customers.controller('CustomersController', ['$scope', '$rootScope', 'CustomerFactory', 'SecurityFactory', '$sessionStorage', function ($scope, $rootScope, CustomerFactory, SecurityFactory, $sessionStorage) {

    $scope.$rootScope = $rootScope;

    $scope.$storage = $sessionStorage;
    $scope.$storage.cacheRefreshInterval = 30; //seconds

    $scope.CustomerFactory = CustomerFactory;
    $scope.SecurityFactory = SecurityFactory;

    $scope.sortableOptions = {
        cursor: 'pointer',
        update: (function (e, ui) {
            alert(ui.item.scope().customer.CustomerName);

            ui.item.sortable.cancel();
        })
    };

    $scope.init = function () {
        
        $scope.getCurrentUserName();

        $scope.loadCustomers();

    }

    $scope.loadCustomers = function () {

        if ($scope.$storage.customers != null && $scope.$storage.customersCacheDate != null && $scope.$storage.appCacheStatus == $rootScope.CacheStatus.Enabled) {
            var cacheDuration = new Date() - new Date($scope.$storage.customersCacheDate);
            cacheDuration = (cacheDuration / 1000);
            if (cacheDuration < $scope.$storage.cacheRefreshInterval) {
                console.log('Using cache. Time remaining: ' + cacheDuration);
                return;
            }
        } 

        console.log('Fetching data from server. Cache has expired or does not exist.');

        var promise = $scope.CustomerFactory.getCustomers();

        promise.then(function (result) {

            $scope.$storage.customers = result.data;
            $scope.$storage.customersCacheDate = new Date();

        },
        function (error) {

            $scope.error = error.message;

        });

    }

    $scope.getCurrentUserName = function () {

        var promise = $scope.SecurityFactory.getCurrentUsername();

        promise.then(function (result) {

            $scope.$storage.currentUsername = result.data;

        },
        function (error) {

            $scope.error = error.message;

        });

    }

}]);