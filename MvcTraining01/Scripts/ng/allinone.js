﻿function CustomerCtrl2($scope, $http) {

    $scope.init = function () {
        $scope.getCustomers();
    }

    $scope.getCustomers = function () {

        var promise = $http.get('../api/v1/customers');

        promise.then(function (result) {

            $scope.customers = result.data;

            //alert($scope.customers.length);

        },
        function (error) {

            $scope.error = error.message;

        });
    }
};