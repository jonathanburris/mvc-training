﻿using System.Web;
using System.Web.Optimization;

namespace MvcTraining01
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui.min-1.11.1.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/ng").Include(
                "~/Scripts/angular.min.js",
                "~/Scripts/angular-ui-router.min.js",
                "~/Scripts/sortable.js",
                "~/Scripts/ngStorage.min.js",
                "~/Scripts/ng/apps/global/app.js",
                "~/Scripts/ng/apps/customers/app.js",
                "~/Scripts/ng/factories/customerFactory.js",
                "~/Scripts/ng/factories/securityFactory.js",
                "~/Scripts/ng/controllers/customers/customersCtrl.js",
                "~/Scripts/ng/controllers/customers/customerCtrl.js",
                "~/Scripts/ng/controllers/customers/testCtrl.js",
                "~/Scripts/ng/controllers/customers/homeCtrl.js",
                "~/Scripts/ng/controllers/customers/footerCtrl.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/font-awesome.min.css"));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            //BundleTable.EnableOptimizations = true;
        }
    }
}
