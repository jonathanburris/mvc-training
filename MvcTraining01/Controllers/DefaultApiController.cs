﻿using MvcTraining01.Models;
using MvcTraining01.Models.DataProviders;
using MvcTraining01.Models.Factories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MvcTraining01.Controllers
{
    [RoutePrefix("api/v1")]
    public class DefaultApiController : ApiController
    {
        #region Customers

        [Authorize]
        [HttpGet, Route("customers")]
        public List<Customer> GetCustomers()
        {
            return CustomersFactory.GetCustomers();
        }

        #region Orders
        #endregion

        #region Invoices
        #endregion

        #endregion


        #region Security
        [Authorize]
        [HttpGet, Route("security/currentusername")]
        public string GetCurrentUsername()
        {
            return RequestContext.Principal.Identity.Name;
        }

        [Authorize]
        [HttpGet, Route("security/currentuser")]
        public User GetCurrentUser()
        {
            return UsersFactory.GetUser(RequestContext.Principal.Identity.Name);
        }

        [Authorize]
        [HttpGet, Route("security/users")]
        public Users GetUsers()
        {
            return UsersFactory.GetUsers();
        }

        [Authorize]
        [HttpGet, Route("security/users/{username}")]
        public User GetUser(string username)
        {
            User user = UsersFactory.GetUser(username);
            Console.WriteLine(string.Format("{0} is a {1}", user.DisplayName, user.Role.ToString()));
            return user;
        }

        #endregion
    }
}
