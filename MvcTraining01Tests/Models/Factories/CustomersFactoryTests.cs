﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MvcTraining01.Models.Factories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcTraining01.Models.DataProviders;
namespace MvcTraining01.Models.Factories.Tests
{
    [TestClass()]
    public class CustomersFactoryTests
    {
        [TestMethod()]
        public void GetCustomersTest()
        {
            List<Customer> customers = CustomersFactory.GetCustomers(new MockCustomersDataProvider());
            Assert.AreEqual(4, customers.Count);
        }
    }
}
